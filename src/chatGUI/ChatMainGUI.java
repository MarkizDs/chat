package chatGUI;

import java.io.IOException;
import java.net.UnknownHostException;

public class ChatMainGUI {

    public static void main(String[] args) throws UnknownHostException, IOException {
        new ChatFrame();
    }
}
